# Подготовка модели

## Вычисление разницы между весами моделей (создание diff файла)

> diff файл - разница весов модели между новым и старым файлом h5.

Находясь в service_main необходимо ввести команду:
```python3
python3 utils/create_diff.py -o <путь до старой модели> -n <путь до новой модели>
```
После данной команды в папке weights создаться файл param_diff.h5, либо в указанной папке пользователем.

Пример команды:
```python3
python3 utils/create_diff.py -o /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/neural_service/trainingloop_tf/weights/loss_1.4210474491119385_MobileNetv2.h5 -n /Users/nikitakamenev/Documents/scince/aerospace_computing_systems/neural_service/service_main/weights/bestmodel.h5
```
