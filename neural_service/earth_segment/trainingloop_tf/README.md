##### Запуск локально

### Настройка окружения

Сначала создать и активировать venv:

```bash
python3 -m venv venv
. venv/bin/activate
```

1. Установка зависимостей ```make install```
2. Последнияя обученная модель расположена в папке weights
3. Необходимо в файле ```config.py```, расположенного в папке ```config```, указать путь до изображений в поле IMAGE_DIR.
4. Необходимо в файле ```config.py```, расположенного в папке ```config```, указать путь до таблицы с метками.
<br /> Таблица с метками: 1-ый столбец путь до **изображений**, 2-ой столбец название **метки**.

***Пример***

**Config файл**
```
N_EPOCHS = 20 - количество эпох

IMAGE_DIR = '/Volumes/T7/dataset/raw/' - путь до изображений
LABELS_PATH = '/Volumes/T7/dataset/raw/labels.csv' - путь до меток
```

**Таблица с метками labels.csv**

| image_name | tags                            |
| ---------- | ------------------------------- |
| train_0    | haze primary                    |
| train_1    | agriculture clear primary water |
| train_2    | clear primary                   |
| train_17   | partly_cloudy primary           |
| train_26   | cloudy                          |

6. Запуск обучения модели ```python train.py```

*Лучшие модели сохраняются в папке weights*
