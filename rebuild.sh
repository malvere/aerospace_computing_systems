# Сбросить данные БД и предыдущий незавершенный расчёт модели КА
docker compose down --volumes

# https://techrocks.ru/2022/04/16/docker-build-cache/
# docker-compose build --no-cache

docker compose build
