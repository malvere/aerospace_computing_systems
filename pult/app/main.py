import time
from typing import Union

import can
import uvicorn
from fastapi import FastAPI, Form, Request
from fastapi.encoders import jsonable_encoder
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from cant import cant

CAN_PROTOCOL_FILENAME = "capy/cantid"

# название приложения
app = FastAPI()
cant_client = cant()

# указываем директорию, где будут лежать шаблоны страниц, она должна лежать на одном
# уровне с папкой app
templates = Jinja2Templates(directory="templates")


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/hlo/", response_class=HTMLResponse)
# ВНИМАНИЕ должен быть выше импортирован Request из FastAPI
async def home(request: Request):
    data = {"page": "Home page"}
    return templates.TemplateResponse("page.html", {"request": request, "data": data})


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


@app.get("/page/{page_name}")
async def page(request: Request, page_name: str):
    data = {"page": page_name}
    txt = "das is einfache text"
    return templates.TemplateResponse(
        "page.html", {"request": request, "data": data, "txt": txt}
    )


"""
здесь request - это собственно запрос страницы

"data": data - пара ключ/значение, где ключ - имя переменной в шаблоне, а значение имя
переменной в питоне

"""


@app.get("/twoforms", response_class=HTMLResponse)
def form_get(request: Request):
    result = "Задайте в полях число"
    return templates.TemplateResponse(
        "twoform.html", context={"request": request, "result": result}
    )


@app.post("/form1", response_class=HTMLResponse)
def form_post1(request: Request, number: int = Form(...)):
    result = number + 2
    return templates.TemplateResponse(
        "twoform.html",
        context={"request": request, "result": result, "yournum": number},
    )


@app.post("/form2", response_class=HTMLResponse)
def form_post2(request: Request, number: int = Form(...), action: str = Form(...)):
    # print('post_form2')
    if action == "a100":
        result = number + 100
    elif action == "m100":
        result = number * 100
    return templates.TemplateResponse(
        "twoform.html",
        context={"request": request, "result": result, "yournum": number},
    )


FieldNames = [
    "Repeat",
    "Flags",
    "ID_CRC8",
    "Reciever_NET",
    "Reciever",
    "Sender_NET",
    "Sender",
    "Type",
    "DATA",
]


@app.get("/pult")
def pult_get():
    T1 = '<h3>ПУЛЬТ УПРАВЛЕНИЯ: </h3> <form action="/pult" method="post">'
    b = 0
    for a in FieldNames:
        b += 1
        T1 += '<label for="' + a + '">' + a + ":</label><br>"
        T1 += (
            '<input type="number" name="'
            + a
            + '" value="'
            + str(b)
            + '" required label="'
            + a
            + '" /><br>'
        )

    T1 += '<input type="submit" >'
    T1 += "</form>"

    # print(T1)

    html_content_B = """
    <html>
        <head>
            <title>Some HTML in here</title>
        </head>

    """

    html_content_E = """
        <body>
            <h1>OK!</h1>
        </body>
    </html>
    """

    html_content = html_content_B + T1 + html_content_E
    return HTMLResponse(content=html_content, status_code=200)


@app.post("/pult")
async def check(request: Request):
    print("Start PULT post")
    da = await request.form()
    da = jsonable_encoder(da)
    # print(da)
    lda = len(da)
    print("da length=", lda)
    for i in range(1, lda):
        print(FieldNames[i], " : ", da[FieldNames[i]])

    print("Start CAN")

    # TODO перенести код ниже в библиотеку cant
    # >>>
    with open(CAN_PROTOCOL_FILENAME) as protocol_file:
        lines = protocol_file.readlines()

    field_names, field_lengths, field_keys = zip(*(line.split() for line in lines))

    # TODO почему в pyt.py не используются бинарные строки, а здесь используются?
    # field_names = [f_name.encode("ascii") for f_name in field_names]
    print("SetID")
    cant_client.SetId(field_names, field_lengths, field_keys)

    print("setID ok")
    # <<<

    print("setAttrByName=", cant_client.setAttrByName(b"Sender", 1))
    print("setAttrByName=", cant_client.setAttrByName(b"Sender_NET", 2))

    print("setAttrByName=", cant_client.setAttrByName(b"Reciever", 12))
    print("setAttrByName=", cant_client.setAttrByName(b"Reciever_NET", 3))

    print("setAttrByName=", cant_client.setAttrByName(b"Type", 0))
    print("setAttrByName=", cant_client.setAttrByName(b"ID_CRC8", 63))

    print("codeId()      ", cant_client.codeId())

    bus = can.Bus(interface="socketcan", channel="vcan0", bitrate=250000)

    print("Start Send")

    for i in range(10):
        time.sleep(1)
        print("step=", i)
        print("getAdr()    ", cant_client.getAdr())
        msg = can.Message(
            arbitration_id=cant_client.getAdr(), data=b"AAAABBBB", is_extended_id=True
        )
        try:
            bus.send(msg)
            # print(f"Message sent on {bus.channel_info}")
        except can.CanError:
            print("Message NOT sent")

    print("Finish!")

    return da


if __name__ == "__main__":
    uvicorn.run(app=app, host="0.0.0.0", port=8000)
